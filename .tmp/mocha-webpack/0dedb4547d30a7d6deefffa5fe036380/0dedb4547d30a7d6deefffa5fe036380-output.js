/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("assert");

/***/ }),
/* 1 */
/***/ (function(module, exports) {


function UserManager(users, groups) {
    // there is no reason for this image, just to make the page look like the real thing :)
    var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';


    /**
     * get the list of existing users with an option to get users
     * from specific group
     * @param {Array} users 
     * @param {Array} groups 
     * @return {Array} 
     */
    var getUsers = function (filter) {
        // if the filter option is All then return all users
        if (filter === 'All') return users;

        var filteredUsers = [];
        groups.forEach(function (group) {
            if (group.name == filter) {
                filteredUsers = users.filter(function (user) {
                    if (user.groups.indexOf(group.id) > -1) return user;
                });
            }
        });
        return filteredUsers;
    }

    /**
     * add a users to the list of users
     * @param {Array} users 
     */
    var addUser = function (userName, job) {
        var user = {
            // the id generation here is just incrementing the previou user
            id: users[users.length - 1].id + 1,
            name: userName,
            img: commonImage,
            jobTitle: job,
            groups: []
        }
        users.push(user);
    }

    /**
     * Remove user from the existing list of users
     * @param {Array} users 
     */
    var removeUser = function (name) {
        users.forEach(function (user, i) {
            if (user.name === name) {
                users.splice(i, 1);
            }
        }, this);
    }

    /**
     * get the list of groups
     * @param {Array} groups 
     */
    var getGroups = function () {
        return groups;
    }

    /**
     * Add groups to the list of existing groups
     * @param {Array} groups 
     */
    var addGroup = function (groupName) {
        groups.push({
            id: groups[groups.length - 1].id +
            1, name: groupName
        });
    }

    /**
     * remove a group from the existing list groups
     * @param {Array} groups 
     */
    var removeGroup = function (groupName) {
        var groupId;

        // remove the group first
        groups.forEach(function (group, i) {
            if (group.name == groupName) {
                groupId = group.id
                groups.splice(i, 1);
            }
        });

        // from the group referernce from all users
        users.forEach(function (user) {
            if (user.groups.indexOf(groupId) > -1)
                user.groups.splice(groupId, 1);
        }, this);
    }

    /**
     * add a user to one of the existing groups
     * @param {Array} users 
     * @param {Array} groups 
     */
    var addUserToGroup = function (name, groupName) {
        users.forEach(function (user) {
            if (user.name === name) {
                groups.forEach(function (group) {
                    if (group.name == groupName &&
                        groups.indexOf(group.id) == -1)
                        user.groups.push(group.id);
                })
            }
        }, this);
    }

    /**
     *  remove specified group id from a speicifed user
     * @param {Array} users 
     * @param {Array} groups 
     */
    var removeUserFromGroup = function (name, groupName) {
        users.forEach(function (user) {
            if (user.name === name) {
                groups.forEach(function (group) {
                    if (group.name === groupName &&
                        user.groups.indexOf(group.id) > -1)
                        user.groups.splice(user.groups.indexOf(group.id), 1);
                })
            }
        }, this);
    }

    return {
        getUsers: getUsers,
        addUser: addUser,
        removeUser: removeUser,
        getGroups: getGroups,
        addGroup: addGroup,
        removeGroup: removeGroup,
        addUserToGroup: addUserToGroup,
        removeUserFromGroup: removeUserFromGroup
    }
}

module.exports = UserManager;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {


    var testsContext = __webpack_require__(3);

    var runnable = testsContext.keys();

    runnable.forEach(testsContext);
    

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./test/botkit-routes.test.js": 4,
	"./test/users-manager.test.js": 8
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 3;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var assert = __webpack_require__(0);
var botkitRoutes = __webpack_require__(5);
var Botmock = __webpack_require__(7);
var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';

var users = [{
    id: 1,
    name: 'Omran',
    img: commonImage,
    jobTitle: 'Web Developer',
    groups: [1]
}, {
    id: 2,
    name: 'John Doe',
    img: commonImage,
    jobTitle: 'Front-end dev',
    groups: [2]
}];

var groups = [
    {
        active: "active",
        name: "All"
    },
    {
        id: 1,
        name: 'Front-end devs'
    },
    {
        id: 2,
        name: 'Back-end devs'
    },
    {
        id: 3,
        name: 'Sysadmins'
    }
]

var channel = 0;

var hear = function (texts, assertionIndex) {
    var hearObj = {
        user: 'someUser',
        channel: 'channel',
        messages: ['hello']
    };
    if (typeof texts === 'string')
        hearObj.messages.push({
            text: texts,
            isAssertion: true
        });
    else
        texts.forEach(function (text, i) {
            if (i == assertionIndex)
                hearObj.messages.push({
                    text: text,
                    isAssertion: true
                })
            else
                hearObj.messages.push({
                    text: text
                })
        }, this);
    return hearObj;
}

describe("controller tests", function () {

    afterEach(function () {
		this.controller.shutdown();
    });

    beforeEach(function () {
        this.controller = Botmock({debug: false});
        // type can be ‘slack’, facebook’, or null
        this.bot = this.controller.spawn({ type: null });
        botkitRoutes(this.controller);
    });

    it('should replay with a list of all users', function () {
        return this.bot.usersInput([hear('list users')]
        ).then((message) => {
            assert.deepEqual(message.data, users);
            assert.equal(message.option, 'users');
        })
    });

    it('should replay with a list of all users', function () {
        return this.bot.usersInput([hear('list groups')]
        ).then((message) => {
            assert.deepEqual(message.data, groups);
            assert.equal(message.option, 'groups');
        })
    });


    it('should ask for the user name when create user is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 0)]
        ).then((message) => {
            assert.equal(message.text, 'what is the user name?');
        })
    });

    it('should ask for the user role when create user name is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 1)]
        ).then((message) => {
            assert.equal(message.text, 'what is the user role?');
        })
    });

    it('should return user added when the role is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 2)]
        ).then((message) => {
            assert.equal(message.text, 'User Added.');
        })
    });

    it('should ask for the group name when create group', function () {
        return this.bot.usersInput([hear(['create group', 'Network programmers'], 0)]
        ).then((message) => {
            assert.equal(message.text, 'What is the group name?');
        })
    });

    it('should say user added when the group is add when create group', function () {
        return this.bot.usersInput([hear(['create group', 'Network programmers'], 1)]
        ).then((message) => {
            assert.equal(message.text, 'Group Added.');
        })
    });

    it('should assign a user to group', function () {
        return this.bot.usersInput([hear('assign Omran to Back-end devs')]
        ).then((message) => {
            assert.equal(message.text, 'User added to the specified group');
        })
    });

    it('should filter users by group names', function () {
        return this.bot.usersInput([hear('filter Front-end devs')]
        ).then((message) => {
            assert.deepEqual(message.data, [{
                id: 1,
                name: 'Omran',
                img: commonImage,
                jobTitle: 'Web Developer',
                groups: [1, 2]
            }]);
        })
    });

    it('should remove a user from group', function () {
        return this.bot.usersInput([hear('remove Omran from Front-end devs')]
        ).then((message) => {
            assert.equal(message.text, 'User removed from group Front-end devs');
        })
    });

    it('should remove user', function () {
        return this.bot.usersInput([hear('remove user Omran')]
        ).then((message) => {
            assert.deepEqual(message.data, [{
                id: 2,
                name: 'John Doe',
                img: commonImage,
                jobTitle: 'Front-end dev',
                groups: [2]
            },
            {
                id: 3,
                name: 'Omran Abazid',
                img: commonImage,
                jobTitle: 'Web developer',
                groups: []
            }]);
            assert.equal(message.option, 'users');
        })
    });

    it('should remove group', function () {
        return this.bot.usersInput([hear('remove group Front-end devs')]
        ).then((message) => {
            assert.deepEqual(message.data, [ { name: 'All', active: 'active' },
            { id: 2, name: 'Back-end devs' },
            { id: 3, name: 'Sysadmins' },
            { id: 4, name: 'Network programmers' } ]);
        assert.equal(message.option, 'groups');
    })
});


});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var replayManager = __webpack_require__(6)();

/**
 * handle the chat messages routes
 * @param {object} controller 
 */
function BotkitRouts(controller) {

    controller.hears(['list users'], 'message_received', replayManager.listUsers);
    controller.hears(['list groups'], 'message_received', replayManager.listGroups);
    controller.hears(['create user'], 'message_received', replayManager.createUser);
    controller.hears(['create group'], 'message_received', replayManager.createGroup);
    controller.hears(['assign (.*) to (.*)'], 'message_received', replayManager.assignUserToGroup);
    controller.hears(['filter (.*)'], 'message_received', replayManager.filterUsers);
    controller.hears(['remove (.*) from (.*)'], 'message_received', replayManager.removeUserFromGroup);
    controller.hears(['remove user (.*)'], 'message_received', replayManager.removeUser);
    controller.hears(['remove group (.*)'], 'message_received', replayManager.removeGroup);

}
module.exports = BotkitRouts;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var emptyUsersManager = __webpack_require__(1);

function ReplayManager() {

    // useless image just to make the ui look better
    var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';


    // data is stored privatly using normal variables
    // represents the database part of the project
    var users = [{
        id: 1,
        name: 'Omran',
        img: commonImage,
        jobTitle: 'Web Developer',
        groups: [1]
    }, {
        id: 2,
        name: 'John Doe',
        img: commonImage,
        jobTitle: 'Front-end dev',
        groups: [2]
    }];

    // geeky groups :)
    var groups = [
        {
            id: 1,
            name: 'Front-end devs'
        },
        {
            id: 2,
            name: 'Back-end devs'
        },
        {
            id: 3,
            name: 'Sysadmins'
        }
    ]

    // the user manager is responsible to any opertation regarding the data
    var usersManager = emptyUsersManager(users, groups);


    /**
     * sends to the client All the users when list users is typed
     * @param {*} bot 
     * @param {*} message 
     */
    var listUsers = function (bot, message) {
        var msg = {
            data: usersManager.getUsers('All'),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * sends a list All the groups
     * @param {*} bot 
     * @param {*} message 
     */
    var listGroups = function (bot, message) {
        var menu = usersManager.getGroups().slice(0);
        menu.unshift({ name: 'All', "active": "active" });
        var msg = {
            data: menu,
            option: 'groups'
        }
        bot.reply(message, msg);
    }

    /**
     * create a user after running a conversation 
     * to ask the user about the data needed
     * @param {*} bot 
     * @param {*} message 
     */
    var createUser = function (bot, message) {
        bot.startConversation(message, function (err, convo) {
            if (err) return console.log(err);
            convo.ask("what is the user name?", function (res, convo) {
                var userName = res.text;
                convo.ask("what is the user role?", function (res, convo) {
                    var job = res.text;
                    usersManager.addUser(userName, job);
                    convo.say("User Added.");
                    convo.next();
                });
                convo.next();
            });
        });
    }

    /**
     * add a group and replay with goup added 
     * @param {*} bot 
     * @param {*} message 
     */
    var createGroup = function (bot, message) {
        bot.startConversation(message, function (err, convo) {
            if (err) return console.log(err);
            convo.ask("What is the group name?", function (res, convo) {
                var groupName = res.text;
                usersManager.addGroup(groupName);
                convo.say("Group Added.");
                var menu = usersManager.getGroups().slice(0);
                menu.unshift({ name: 'All', "active": "active" });
                var msg = {
                    data: menu,
                    option: 'groups'
                }
                bot.reply(message, msg);
                convo.next();
            });
        });
    }


    /**
     * extracts the user and group and 
     * call the user manager to assign the user
     * to the group
     * @param {*} bot 
     * @param {*} message 
     */
    var assignUserToGroup = function (bot, message) {
        var name = message.match[1];
        var groupName = message.match[2];
        usersManager.addUserToGroup(name, groupName);
        bot.reply(message, 'User added to the specified group');
    }

    /**
     * replay with the list of filtered users by group
     * when filter is typed or a button is clicked
     * @param {*} bot 
     * @param {*} message 
     */
    var filterUsers = function (bot, message) {
        var groupName = message.match[1];
        var msg = {
            data: usersManager.getUsers(groupName),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * remove user from group
     * @param {*} bot 
     * @param {*} message 
     */
    var removeUserFromGroup = function (bot, message) {
        var name = message.match[1];
        var groupName = message.match[2];
        usersManager.removeUserFromGroup(name, groupName);
        bot.reply(message, 'User removed from group ' + groupName);
    }


    /**
     * delete an existing user
     * @param {*} bot 
     * @param {*} message 
     */
    var removeUser = function (bot, message) {
        var name = message.match[1];
        usersManager.removeUser(name);
        var msg = {
            data: usersManager.getUsers('All'),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * remove a group
     * @param {*} bot 
     * @param {*} message 
     */
    var removeGroup = function (bot, message) {
        var groupName = message.match[1];
        usersManager.removeGroup(groupName);
        var menu = usersManager.getGroups().slice(0);
        menu.unshift({ name: 'All', "active": "active" });
        var msg = {
            data: menu,
            option: 'groups'
        }
        bot.reply(message, msg);
    }

    return {
        listUsers: listUsers,
        listGroups: listGroups,
        createUser: createUser,
        createGroup: createGroup,
        assignUserToGroup: assignUserToGroup,
        filterUsers: filterUsers,
        removeUserFromGroup: removeUserFromGroup,
        removeUser: removeUser,
        removeGroup: removeGroup
    }
}

module.exports = ReplayManager;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("botkit-mock");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var assert = __webpack_require__(0);
var emptyManager = __webpack_require__(1);
var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';

var users = [{
  id: 1,
  name: 'Omran',
  img: commonImage,
  jobTitle: 'Web Developer',
  groups: [2]
}, {
  id: 2,
  name: 'John Doe',
  img: commonImage,
  jobTitle: 'Front-end dev',
  groups: [1]
}];

var groups = [
  {
    id: 1,
    name: 'Front-end devs'
  },
  {
    id: 2,
    name: 'Back-end devs'
  },
  {
    id: 3,
    name: 'Sysadmins'
  }
]

var usersManager = emptyManager(users, groups);



describe('usersManager', function () {
  it('should return a list of all users', function () {
    
    assert.deepEqual(usersManager.getUsers('All'), users);
  });

  it('should return a list of front-end users', function () {
    
    
    var results = usersManager.getUsers('Front-end devs');

    assert.deepEqual(results, [{
      id: 2,
      name: 'John Doe',
      img: commonImage,
      jobTitle: 'Front-end dev',
      groups: [1]
    }]);
  });

  it('it should return the list of existing groups', function () {
    assert.deepEqual(usersManager.getGroups(), groups);
  });

  it('it should add new user', function () {
    usersManager.addUser("Mark", "Web Developer");
    var addedUser = {
      id: 3,
      name: 'Mark',
      img: commonImage,
      jobTitle: 'Web Developer',
      groups: []
    };
    assert.deepEqual(usersManager.getUsers('All')[2], addedUser);
  });

  it('it should add new group', function () {
    usersManager.addGroup("new group");
    var addedGroup = {
      id: 4,
      name: 'new group'
    };
    assert.deepEqual(usersManager.getGroups()[3], addedGroup);
  });

  it("shoud remove user specified", function () {
    usersManager.removeUser("Omran");
    assert.notDeepEqual(usersManager.getUsers('All')[0], {
      id: 1,
      name: 'Omran',
      img: commonImage,
      jobTitle: 'Web Developer',
      groups: [2]
    });
  });

  it("shoud remove group specified", function () {
    usersManager.removeGroup("Front-end devs");
    assert.notDeepEqual(usersManager.getGroups()[0], {
      id: 1,
      name: 'Front-end devs'
    });
  });

  it("it should add user to a group", function () {
    usersManager.addUserToGroup("John Doe", "Back-end devs");
    assert.deepEqual(usersManager.getUsers('All')[0].groups, [1, 2]);
  });


  it("it should remove user from a group", function () {
    usersManager.removeUserFromGroup("John Doe", "Back-end devs");
    assert.deepEqual(usersManager.getUsers('All')[0].groups, [1]);
  });


});




/***/ })
/******/ ]);