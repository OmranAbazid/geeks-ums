var assert = require('assert');
var botkitRoutes = require('../backend/botkit-routes');
var Botmock = require('botkit-mock');
var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';

var users = [{
    id: 1,
    name: 'Omran',
    img: commonImage,
    jobTitle: 'Web Developer',
    groups: [1]
}, {
    id: 2,
    name: 'John Doe',
    img: commonImage,
    jobTitle: 'Front-end dev',
    groups: [2]
}];

var groups = [
    {
        active: "active",
        name: "All"
    },
    {
        id: 1,
        name: 'Front-end devs'
    },
    {
        id: 2,
        name: 'Back-end devs'
    },
    {
        id: 3,
        name: 'Sysadmins'
    }
]

var channel = 0;

var hear = function (texts, assertionIndex) {
    var hearObj = {
        user: 'someUser',
        channel: 'channel',
        messages: ['hello']
    };
    if (typeof texts === 'string')
        hearObj.messages.push({
            text: texts,
            isAssertion: true
        });
    else
        texts.forEach(function (text, i) {
            if (i == assertionIndex)
                hearObj.messages.push({
                    text: text,
                    isAssertion: true
                })
            else
                hearObj.messages.push({
                    text: text
                })
        }, this);
    return hearObj;
}

describe("controller tests", function () {

    afterEach(function () {
		this.controller.shutdown();
    });

    beforeEach(function () {
        this.controller = Botmock({debug: false});
        // type can be ‘slack’, facebook’, or null
        this.bot = this.controller.spawn({ type: null });
        botkitRoutes(this.controller);
    });

    it('should replay with a list of all users', function () {
        return this.bot.usersInput([hear('list users')]
        ).then((message) => {
            assert.deepEqual(message.data, users);
            assert.equal(message.option, 'users');
        })
    });

    it('should replay with a list of all users', function () {
        return this.bot.usersInput([hear('list groups')]
        ).then((message) => {
            assert.deepEqual(message.data, groups);
            assert.equal(message.option, 'groups');
        })
    });


    it('should ask for the user name when create user is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 0)]
        ).then((message) => {
            assert.equal(message.text, 'what is the user name?');
        })
    });

    it('should ask for the user role when create user name is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 1)]
        ).then((message) => {
            assert.equal(message.text, 'what is the user role?');
        })
    });

    it('should return user added when the role is typed', function () {
        return this.bot.usersInput([hear(['create user', 'Omran Abazid', 'Web developer'], 2)]
        ).then((message) => {
            assert.equal(message.text, 'User Added.');
        })
    });

    it('should ask for the group name when create group', function () {
        return this.bot.usersInput([hear(['create group', 'Network programmers'], 0)]
        ).then((message) => {
            assert.equal(message.text, 'What is the group name?');
        })
    });

    it('should say user added when the group is add when create group', function () {
        return this.bot.usersInput([hear(['create group', 'Network programmers'], 1)]
        ).then((message) => {
            assert.equal(message.text, 'Group Added.');
        })
    });

    it('should assign a user to group', function () {
        return this.bot.usersInput([hear('assign Omran to Back-end devs')]
        ).then((message) => {
            assert.equal(message.text, 'User added to the specified group');
        })
    });

    it('should filter users by group names', function () {
        return this.bot.usersInput([hear('filter Front-end devs')]
        ).then((message) => {
            assert.deepEqual(message.data, [{
                id: 1,
                name: 'Omran',
                img: commonImage,
                jobTitle: 'Web Developer',
                groups: [1, 2]
            }]);
        })
    });

    it('should remove a user from group', function () {
        return this.bot.usersInput([hear('remove Omran from Front-end devs')]
        ).then((message) => {
            assert.equal(message.text, 'User removed from group Front-end devs');
        })
    });

    it('should remove user', function () {
        return this.bot.usersInput([hear('remove user Omran')]
        ).then((message) => {
            assert.deepEqual(message.data, [{
                id: 2,
                name: 'John Doe',
                img: commonImage,
                jobTitle: 'Front-end dev',
                groups: [2]
            },
            {
                id: 3,
                name: 'Omran Abazid',
                img: commonImage,
                jobTitle: 'Web developer',
                groups: []
            }]);
            assert.equal(message.option, 'users');
        })
    });

    it('should remove group', function () {
        return this.bot.usersInput([hear('remove group Front-end devs')]
        ).then((message) => {
            assert.deepEqual(message.data, [ { name: 'All', active: 'active' },
            { id: 2, name: 'Back-end devs' },
            { id: 3, name: 'Sysadmins' },
            { id: 4, name: 'Network programmers' } ]);
        assert.equal(message.option, 'groups');
    })
});


});