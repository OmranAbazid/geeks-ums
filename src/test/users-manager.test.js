var assert = require('assert');
var emptyManager = require('../backend/users-manager');
var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';

var users = [{
  id: 1,
  name: 'Omran',
  img: commonImage,
  jobTitle: 'Web Developer',
  groups: [2]
}, {
  id: 2,
  name: 'John Doe',
  img: commonImage,
  jobTitle: 'Front-end dev',
  groups: [1]
}];

var groups = [
  {
    id: 1,
    name: 'Front-end devs'
  },
  {
    id: 2,
    name: 'Back-end devs'
  },
  {
    id: 3,
    name: 'Sysadmins'
  }
]

var usersManager = emptyManager(users, groups);



describe('usersManager', function () {
  it('should return a list of all users', function () {
    
    assert.deepEqual(usersManager.getUsers('All'), users);
  });

  it('should return a list of front-end users', function () {
    
    
    var results = usersManager.getUsers('Front-end devs');

    assert.deepEqual(results, [{
      id: 2,
      name: 'John Doe',
      img: commonImage,
      jobTitle: 'Front-end dev',
      groups: [1]
    }]);
  });

  it('it should return the list of existing groups', function () {
    assert.deepEqual(usersManager.getGroups(), groups);
  });

  it('it should add new user', function () {
    usersManager.addUser("Mark", "Web Developer");
    var addedUser = {
      id: 3,
      name: 'Mark',
      img: commonImage,
      jobTitle: 'Web Developer',
      groups: []
    };
    assert.deepEqual(usersManager.getUsers('All')[2], addedUser);
  });

  it('it should add new group', function () {
    usersManager.addGroup("new group");
    var addedGroup = {
      id: 4,
      name: 'new group'
    };
    assert.deepEqual(usersManager.getGroups()[3], addedGroup);
  });

  it("shoud remove user specified", function () {
    usersManager.removeUser("Omran");
    assert.notDeepEqual(usersManager.getUsers('All')[0], {
      id: 1,
      name: 'Omran',
      img: commonImage,
      jobTitle: 'Web Developer',
      groups: [2]
    });
  });

  it("shoud remove group specified", function () {
    usersManager.removeGroup("Front-end devs");
    assert.notDeepEqual(usersManager.getGroups()[0], {
      id: 1,
      name: 'Front-end devs'
    });
  });

  it("it should add user to a group", function () {
    usersManager.addUserToGroup("John Doe", "Back-end devs");
    assert.deepEqual(usersManager.getUsers('All')[0].groups, [1, 2]);
  });


  it("it should remove user from a group", function () {
    usersManager.removeUserFromGroup("John Doe", "Back-end devs");
    assert.deepEqual(usersManager.getUsers('All')[0].groups, [1]);
  });


});


