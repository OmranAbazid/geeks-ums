import renderUsers from './views/render-users'
import rendergroups from './views/render-groups'

/**
 * select the right rendering format according to the option
 * recieved by the server 
 */
export default function (data, option) {
    switch(option){
        case 'users':
            renderUsers(data);
        break;
        case 'groups':
            rendergroups(data);
        break;
        case 'text-message':
            document.querySelector('#container')
            .innerHTML = `<div class="card">${data}</div>`;
        break;
        default: 
            renderUsers(data.users), rendergroups(data.groups);
    }
}