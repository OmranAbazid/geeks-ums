import handleUi from './handle-ui';
import io from 'socket.io-client';
import renderUi from './render-ui';
const socket = io('http://localhost:3000');


/**
 *  Caching the dom
 */
const chatInput = document.getElementById('chat-input');
const groups = document.querySelector('#groups');


/**
 *  Events
 */
groups.addEventListener('click', handleButtonClick)
chatInput.addEventListener('keydown', handleInputEnterKey);
socket.on('chat message', render);


/**
 *  Event Handlers
 */


 /**
  * when users clicks on specific group from the ui
  */
function handleButtonClick(e) {
    const groupName = e.target.textContent.replace(/\n/g, '').trim();
    if (e.target.tagName === "BUTTON")
        socket.emit('chat message', `filter ${groupName}`);
}

/**
 * when user presses enter it emits the input command
 *  through the socket to the server
 */
function handleInputEnterKey(e) {
    if (e.keyCode == 13 && !e.shiftKey) {
        socket.emit('chat message', e.target.value);
        e.target.value = '';
        e.preventDefault();
    }
}

/**
 * render the recieved data from the socket in the right format
 */
function render(msg) {
    if (typeof msg.data == "undefined")
        return renderUi(msg.text, "text-message");
    renderUi(msg.data, msg.option);
}

/**
 * render the initial UI by sending list users 
 * and list groups to the socket
 * it also enables the burger menu
 *  initilize function
 */
function init() {
    handleUi();
    socket.emit('chat message', 'list users');
    socket.emit('chat message', 'list groups');
}

init();