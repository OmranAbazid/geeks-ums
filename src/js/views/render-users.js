/**
 * render the users into html and insert it to the dom
 * @param {Array} users 
 */
export default function (users) {

    function getRow(users) {
        var rows = [];
        for (var i = 0; i < users.length; i += 2) {
            rows.push(`<div class='row'>${getUser(users[i]) +
                getUser(users[i + 1])}</div>`)
        }
        return rows;
    }

    function getUser(userData) {
        if (userData === undefined) return '';
        return `<div class="six columns">
                        <div class="card">
                            <img src="${userData.img}" alt="">
                            <h4>${userData.name}</h4>
                            <p>${userData.jobTitle}</p>
                        </div>
                    </div>`;
    }

    function getUsersList(users) {
        return `<div class="container">
                  ${getRow(users).join('')}
                </div>`;
    }

    document.querySelector('#container')
        .innerHTML = getUsersList(users);
}