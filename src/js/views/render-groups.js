/**
 * render the groups into html and insert it to the dom
 * @param {Array} groups 
 */
export default function (groups) {

    function getGroup(group) {
        return `<li>
                    <button class='${group.active}'>
                        ${group.name}
                    </button>
                </li>`;
    }

    function getGroupsList(groups) {
        return groups.map(getGroup).join('');
    }

    document.querySelector('#groups')
        .innerHTML = getGroupsList(groups);
}