/**
 * enable the sidenav toggle button on mobile
 */
export default function(){
    // cashing the dom
    
    const button = document.getElementById('burger');
    const sidbar = document.querySelector('.main-sidebar');
    const content = document.querySelector('.content-wrapper');

    // events

    button.addEventListener('click', handleBurgerButtonClick);


    // event handlers

    function handleBurgerButtonClick(){
        sidbar.classList.toggle('side-bar-block')
        content.classList.toggle('content-margin');
    }

};