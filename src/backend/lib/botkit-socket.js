var Botkit = require('botkit').core;


// modified the existing lib module consolebot to make a socket io version of botkit

function TextBot(configuration, io) {


    // Create a core botkit bot
    var text_botkit = Botkit(configuration || {});
    var sock = {};
  
      
    text_botkit.middleware.spawn.use(function(bot, next) {

        io.on('connection', function (socket) {
            sock = socket;
            text_botkit.listenToSocket(bot, socket);

            socket.on('disconnect', function () {
                console.log('user disconnected');
            });

            next();
        });

    });

    text_botkit.defineBot(function(botkit, config) {

        var bot = {
            botkit: botkit,
            config: config || {},
            utterances: botkit.utterances,
        };

        bot.createConversation = function(message, cb) {
            botkit.createConversation(this, message, cb);
        };

        bot.startConversation = function(message, cb) {
            botkit.startConversation(this, message, cb);
        };

        bot.send = function(message, cb) {
            io.emit('chat message', message.data);
            if (cb) {
                cb();
            }
        };

        bot.reply = function(src, resp, cb) {
            var msg = {};

            msg.data = resp;

            msg.channel = src.channel;
            msg.user = src.user;

            bot.say(msg, cb);
        };

        bot.findConversation = function(message, cb) {
            botkit.debug('CUSTOM FIND CONVO', message.user, message.channel);
            for (var t = 0; t < botkit.tasks.length; t++) {
                for (var c = 0; c < botkit.tasks[t].convos.length; c++) {
                    if (
                        botkit.tasks[t].convos[c].isActive() &&
                        botkit.tasks[t].convos[c].source_message.user == message.user
                    ) {
                        botkit.debug('FOUND EXISTING CONVO!');
                        cb(botkit.tasks[t].convos[c]);
                        return;
                    }
                }
            }

            cb();
        };

        return bot;

    });

    text_botkit.listenToSocket = function(bot, socket) {
        text_botkit.startTicking();
        socket.on('chat message', function(msg){
            var message = {
                text: msg,
                user: 'user',
                channel: 'text',
                timestamp: Date.now()
            };
            text_botkit.receiveMessage(bot, message);
          });

    };

    return text_botkit;
};

module.exports = TextBot;