var replayManager = require('./replay-manager')();

/**
 * handle the chat messages routes
 * @param {object} controller 
 */
function BotkitRouts(controller) {

    controller.hears(['list users'], 'message_received', replayManager.listUsers);
    controller.hears(['list groups'], 'message_received', replayManager.listGroups);
    controller.hears(['create user'], 'message_received', replayManager.createUser);
    controller.hears(['create group'], 'message_received', replayManager.createGroup);
    controller.hears(['assign (.*) to (.*)'], 'message_received', replayManager.assignUserToGroup);
    controller.hears(['filter (.*)'], 'message_received', replayManager.filterUsers);
    controller.hears(['remove (.*) from (.*)'], 'message_received', replayManager.removeUserFromGroup);
    controller.hears(['remove user (.*)'], 'message_received', replayManager.removeUser);
    controller.hears(['remove group (.*)'], 'message_received', replayManager.removeGroup);

}
module.exports = BotkitRouts;