var emptyUsersManager = require('./users-manager');

function ReplayManager() {

    // useless image just to make the ui look better
    var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';


    // data is stored privatly using normal variables
    // represents the database part of the project
    var users = [{
        id: 1,
        name: 'Omran',
        img: commonImage,
        jobTitle: 'Web Developer',
        groups: [1]
    }, {
        id: 2,
        name: 'John Doe',
        img: commonImage,
        jobTitle: 'Front-end dev',
        groups: [2]
    }];

    // geeky groups :)
    var groups = [
        {
            id: 1,
            name: 'Front-end devs'
        },
        {
            id: 2,
            name: 'Back-end devs'
        },
        {
            id: 3,
            name: 'Sysadmins'
        }
    ]

    // the user manager is responsible to any opertation regarding the data
    var usersManager = emptyUsersManager(users, groups);


    /**
     * sends to the client All the users when list users is typed
     * @param {*} bot 
     * @param {*} message 
     */
    var listUsers = function (bot, message) {
        var msg = {
            data: usersManager.getUsers('All'),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * sends a list All the groups
     * @param {*} bot 
     * @param {*} message 
     */
    var listGroups = function (bot, message) {
        var menu = usersManager.getGroups().slice(0);
        menu.unshift({ name: 'All', "active": "active" });
        var msg = {
            data: menu,
            option: 'groups'
        }
        bot.reply(message, msg);
    }

    /**
     * create a user after running a conversation 
     * to ask the user about the data needed
     * @param {*} bot 
     * @param {*} message 
     */
    var createUser = function (bot, message) {
        bot.startConversation(message, function (err, convo) {
            if (err) return console.log(err);
            convo.ask("what is the user name?", function (res, convo) {
                var userName = res.text;
                convo.ask("what is the user role?", function (res, convo) {
                    var job = res.text;
                    usersManager.addUser(userName, job);
                    convo.say("User Added.");
                    convo.next();
                });
                convo.next();
            });
        });
    }

    /**
     * add a group and replay with goup added 
     * @param {*} bot 
     * @param {*} message 
     */
    var createGroup = function (bot, message) {
        bot.startConversation(message, function (err, convo) {
            if (err) return console.log(err);
            convo.ask("What is the group name?", function (res, convo) {
                var groupName = res.text;
                usersManager.addGroup(groupName);
                convo.say("Group Added.");
                var menu = usersManager.getGroups().slice(0);
                menu.unshift({ name: 'All', "active": "active" });
                var msg = {
                    data: menu,
                    option: 'groups'
                }
                bot.reply(message, msg);
                convo.next();
            });
        });
    }


    /**
     * extracts the user and group and 
     * call the user manager to assign the user
     * to the group
     * @param {*} bot 
     * @param {*} message 
     */
    var assignUserToGroup = function (bot, message) {
        var name = message.match[1];
        var groupName = message.match[2];
        usersManager.addUserToGroup(name, groupName);
        bot.reply(message, {text: 'User added to the specified group'});
    }

    /**
     * replay with the list of filtered users by group
     * when filter is typed or a button is clicked
     * @param {*} bot 
     * @param {*} message 
     */
    var filterUsers = function (bot, message) {
        var groupName = message.match[1];
        var msg = {
            data: usersManager.getUsers(groupName),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * remove user from group
     * @param {*} bot 
     * @param {*} message 
     */
    var removeUserFromGroup = function (bot, message) {
        var name = message.match[1];
        var groupName = message.match[2];
        usersManager.removeUserFromGroup(name, groupName);
        bot.reply(message, {text: 'User removed from group ' + groupName});
    }


    /**
     * delete an existing user
     * @param {*} bot 
     * @param {*} message 
     */
    var removeUser = function (bot, message) {
        var name = message.match[1];
        usersManager.removeUser(name);
        var msg = {
            data: usersManager.getUsers('All'),
            option: 'users'
        }
        bot.reply(message, msg);
    }

    /**
     * remove a group
     * @param {*} bot 
     * @param {*} message 
     */
    var removeGroup = function (bot, message) {
        var groupName = message.match[1];
        usersManager.removeGroup(groupName);
        var menu = usersManager.getGroups().slice(0);
        menu.unshift({ name: 'All', "active": "active" });
        var msg = {
            data: menu,
            option: 'groups'
        }
        bot.reply(message, msg);
    }

    return {
        listUsers: listUsers,
        listGroups: listGroups,
        createUser: createUser,
        createGroup: createGroup,
        assignUserToGroup: assignUserToGroup,
        filterUsers: filterUsers,
        removeUserFromGroup: removeUserFromGroup,
        removeUser: removeUser,
        removeGroup: removeGroup
    }
}

module.exports = ReplayManager;