
function UserManager(users, groups) {
    // there is no reason for this image, just to make the page look like the real thing :)
    var commonImage = 'https://wrbunderwriting.com/wp-content/themes/wrb/dist/img/person-placeholder.jpg';


    /**
     * get the list of existing users with an option to get users
     * from specific group
     * @param {Array} users 
     * @param {Array} groups 
     * @return {Array} 
     */
    var getUsers = function (filter) {
        // if the filter option is All then return all users
        if (filter === 'All') return users;

        var filteredUsers = [];
        groups.forEach(function (group) {
            if (group.name == filter) {
                filteredUsers = users.filter(function (user) {
                    if (user.groups.indexOf(group.id) > -1) return user;
                });
            }
        });
        return filteredUsers;
    }

    /**
     * add a users to the list of users
     * @param {Array} users 
     */
    var addUser = function (userName, job) {
        var user = {
            // the id generation here is just incrementing the previou user
            id: users[users.length - 1].id + 1,
            name: userName,
            img: commonImage,
            jobTitle: job,
            groups: []
        }
        users.push(user);
    }

    /**
     * Remove user from the existing list of users
     * @param {Array} users 
     */
    var removeUser = function (name) {
        users.forEach(function (user, i) {
            if (user.name === name) {
                users.splice(i, 1);
            }
        }, this);
    }

    /**
     * get the list of groups
     * @param {Array} groups 
     */
    var getGroups = function () {
        return groups;
    }

    /**
     * Add groups to the list of existing groups
     * @param {Array} groups 
     */
    var addGroup = function (groupName) {
        groups.push({
            id: groups[groups.length - 1].id +
            1, name: groupName
        });
    }

    /**
     * remove a group from the existing list groups
     * @param {Array} groups 
     */
    var removeGroup = function (groupName) {
        var groupId;

        // remove the group first
        groups.forEach(function (group, i) {
            if (group.name == groupName) {
                groupId = group.id
                groups.splice(i, 1);
            }
        });

        // from the group referernce from all users
        users.forEach(function (user) {
            if (user.groups.indexOf(groupId) > -1)
                user.groups.splice(groupId, 1);
        }, this);
    }

    /**
     * add a user to one of the existing groups
     * @param {Array} users 
     * @param {Array} groups 
     */
    var addUserToGroup = function (name, groupName) {
        users.forEach(function (user) {
            if (user.name === name) {
                groups.forEach(function (group) {
                    if (group.name == groupName &&
                        groups.indexOf(group.id) == -1)
                        user.groups.push(group.id);
                })
            }
        }, this);
    }

    /**
     *  remove specified group id from a speicifed user
     * @param {Array} users 
     * @param {Array} groups 
     */
    var removeUserFromGroup = function (name, groupName) {
        users.forEach(function (user) {
            if (user.name === name) {
                groups.forEach(function (group) {
                    if (group.name === groupName &&
                        user.groups.indexOf(group.id) > -1)
                        user.groups.splice(user.groups.indexOf(group.id), 1);
                })
            }
        }, this);
    }

    return {
        getUsers: getUsers,
        addUser: addUser,
        removeUser: removeUser,
        getGroups: getGroups,
        addGroup: addGroup,
        removeGroup: removeGroup,
        addUserToGroup: addUserToGroup,
        removeUserFromGroup: removeUserFromGroup
    }
}

module.exports = UserManager;