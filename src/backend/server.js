var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var botkit = require('./lib/botkit-socket');
var botkitRoutes = require('./botkit-routes')

// initilize botkit socket 
var controller = botkit({
  debug: false,
}, io);

var bot = controller.spawn();

// route the botkit messages to the coressponding function
botkitRoutes(controller);

http.listen(3000, function () {
  console.log('listening on *:3000');
});