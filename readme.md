# Geeks UMS
Geeks UMS is a user management system that mixes the normal UI interface with a chat bot.

It is an experiment that might be usefull in the future for using NLP to controll webpages.

## Installation

```sh
git clone <repo>
cd project
npm install
```

- for the client-side

```sh
npm run dev
```

- for the server-side

```sh
npm run server
```

- for the testing

```sh
npm run test
npm run cover
```

## commands
- here is a list of existing command that you can type

```sh
list users
list groups
create user
create group
assign <userName> to <groupName>
filter <groupsName>
remove <userName> from <groupName>
remove user <userName>
remove group <groupName>
```

## Technologies:
- Front-end: intentionaly did not use a framework here
    -Vanilla ES6 on the front-end + socket.io client
- Back-end 
    - express, socket.io, botkit
- testing
    - unit testing: Mocha
    - code coverage: istanbul

## Stories/Features:

- [X] I can see a list of existing users
    -   list users
- [X] I can see a list of existing groups
    -   list groups
- [X] I can create users
    -   create user Omran
- [X] I can create groups
    -   create group front-end devs
- [X] I can assign users to a group they aren’t already part of
    -   add user omran to front-end devs
- [X] I can remove users from a group
    -   remove omran from front-end devs
- [X] I can delete groups when they no longer have members
    -   delete front-end devs
- [X] I can delete users
    -   delete omran


## workflow
- [X] enable burger menu (done through Handle-ui.js)
- [X] Read command from input
- [X] build a socket.io server
- [X] buid a socket.io botkit interface
- [X] code botkit controller to respond to each command
- [X] create a global datastore object to store user and groups
- [X] code a user manager 

## App running Flow
- Front-end
    read input
    send it through socet.io to the server
    recieve a message from the server
    render the data to the web app
- Back-end
    read input
    send it through socet.io to the server
    recieve a message from the server
    render the data to the web app

## datastore
user >-----< Groups

-   users
    -   create user
    -   delete user
-   groups
    -   create groups
    -   delete group

-   update group and user (add user to a group)

```
users:[{
    id:1,
    name: 'omran',
    img: 'https://google.com/omran.jpg',
    jobTitle: 'Web Developer'
    groups: [1]
}]
groups: [{
    id: 1,
    name: 'Front-end devs',
}]
```






