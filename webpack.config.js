var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: ['./src/js/app.js', './src/css/style.css', './src/index.html'],
    output: {
        path: path.resolve(__dirname, 'src/build'),
        filename: 'app.bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                use: 'raw-loader'
            },
            {
                test: /\.css$/,
                use: 'raw-loader'
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, "src"),
        compress: true,
        inline: true,
        port: 8000
    }
};